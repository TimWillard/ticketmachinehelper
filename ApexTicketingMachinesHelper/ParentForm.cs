﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApexTicketingMachinesHelper
{
    public partial class ParentForm : Form
    {
        public ParentForm()
        {
            InitializeComponent();

            if (queryForm == null)
            {
                queryForm = new QueryForm();
                queryForm.MdiParent = this;
                queryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                queryForm.Dock = DockStyle.Fill;
                queryForm.Show();
            }
            else
            {
                queryForm.Activate();
            }

        }

        QueryForm queryForm;
        private void queryFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (queryForm == null)
            {
                queryForm = new QueryForm();
                queryForm.MdiParent = this;
                queryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                queryForm.Dock = DockStyle.Fill;
                queryForm.Show();
            }
            else
            {
                queryForm.Activate();
            }
        }
    }
}
