﻿namespace ApexTicketingMachinesHelper
{
    partial class QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cboxEnvironment = new System.Windows.Forms.ComboBox();
            this.cboxLocation = new System.Windows.Forms.ComboBox();
            this.txtPingFeedback = new System.Windows.Forms.TextBox();
            this.txtComputerName = new System.Windows.Forms.TextBox();
            this.btnTestDataReplicationService = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDivision = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDistrict = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSql = new System.Windows.Forms.TextBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.cboxSql = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnTestAuditDataReplicationService = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtDataRepUrl = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(11, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.dataGridView1.Size = new System.Drawing.Size(1121, 202);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // cboxEnvironment
            // 
            this.cboxEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxEnvironment.FormattingEnabled = true;
            this.cboxEnvironment.Items.AddRange(new object[] {
            "PROD",
            "QA",
            "INT"});
            this.cboxEnvironment.Location = new System.Drawing.Point(23, 57);
            this.cboxEnvironment.Name = "cboxEnvironment";
            this.cboxEnvironment.Size = new System.Drawing.Size(121, 21);
            this.cboxEnvironment.TabIndex = 4;
            this.cboxEnvironment.SelectedValueChanged += new System.EventHandler(this.cboxEnvironment_SelectedValueChanged);
            // 
            // cboxLocation
            // 
            this.cboxLocation.FormattingEnabled = true;
            this.cboxLocation.Location = new System.Drawing.Point(193, 45);
            this.cboxLocation.Name = "cboxLocation";
            this.cboxLocation.Size = new System.Drawing.Size(245, 21);
            this.cboxLocation.TabIndex = 6;
            this.cboxLocation.SelectedIndexChanged += new System.EventHandler(this.cboxLocation_SelectedIndexChanged);
            // 
            // txtPingFeedback
            // 
            this.txtPingFeedback.Location = new System.Drawing.Point(102, 215);
            this.txtPingFeedback.Name = "txtPingFeedback";
            this.txtPingFeedback.ReadOnly = true;
            this.txtPingFeedback.Size = new System.Drawing.Size(141, 20);
            this.txtPingFeedback.TabIndex = 7;
            // 
            // txtComputerName
            // 
            this.txtComputerName.Location = new System.Drawing.Point(102, 96);
            this.txtComputerName.Name = "txtComputerName";
            this.txtComputerName.ReadOnly = true;
            this.txtComputerName.Size = new System.Drawing.Size(141, 20);
            this.txtComputerName.TabIndex = 9;
            // 
            // btnTestDataReplicationService
            // 
            this.btnTestDataReplicationService.Location = new System.Drawing.Point(15, 18);
            this.btnTestDataReplicationService.Name = "btnTestDataReplicationService";
            this.btnTestDataReplicationService.Size = new System.Drawing.Size(278, 38);
            this.btnTestDataReplicationService.TabIndex = 11;
            this.btnTestDataReplicationService.Text = "Test Data Replication Service (8731)";
            this.btnTestDataReplicationService.UseVisualStyleBackColor = true;
            this.btnTestDataReplicationService.Click += new System.EventHandler(this.btnTestDataReplicationService_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Environment:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Computer Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Division:";
            // 
            // txtDivision
            // 
            this.txtDivision.Location = new System.Drawing.Point(102, 120);
            this.txtDivision.Name = "txtDivision";
            this.txtDivision.ReadOnly = true;
            this.txtDivision.Size = new System.Drawing.Size(54, 20);
            this.txtDivision.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Location:";
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(102, 167);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(54, 20);
            this.txtLocation.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "District:";
            // 
            // txtDistrict
            // 
            this.txtDistrict.Location = new System.Drawing.Point(102, 144);
            this.txtDistrict.Name = "txtDistrict";
            this.txtDistrict.ReadOnly = true;
            this.txtDistrict.Size = new System.Drawing.Size(54, 20);
            this.txtDistrict.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(102, 190);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(141, 20);
            this.txtDescription.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Location:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtComputerName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtPingFeedback);
            this.panel1.Controls.Add(this.txtDivision);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtDistrict);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtLocation);
            this.panel1.Controls.Add(this.txtDescription);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cboxLocation);
            this.panel1.Location = new System.Drawing.Point(12, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 262);
            this.panel1.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "IP / Ping:";
            // 
            // txtSql
            // 
            this.txtSql.Location = new System.Drawing.Point(13, 50);
            this.txtSql.Multiline = true;
            this.txtSql.Name = "txtSql";
            this.txtSql.Size = new System.Drawing.Size(324, 133);
            this.txtSql.TabIndex = 1;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(217, 208);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(121, 23);
            this.btnQuery.TabIndex = 3;
            this.btnQuery.Text = "Query";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cboxSql
            // 
            this.cboxSql.FormattingEnabled = true;
            this.cboxSql.Location = new System.Drawing.Point(13, 18);
            this.cboxSql.Name = "cboxSql";
            this.cboxSql.Size = new System.Drawing.Size(324, 21);
            this.cboxSql.TabIndex = 5;
            this.cboxSql.SelectedValueChanged += new System.EventHandler(this.cboxSql_SelectedValueChanged);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(1087, 554);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cboxSql);
            this.panel2.Controls.Add(this.txtSql);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Location = new System.Drawing.Point(808, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(354, 262);
            this.panel2.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Ticketing System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(827, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "SQL Queries";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(499, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Network";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnTestAuditDataReplicationService);
            this.panel3.Controls.Add(this.btnTestDataReplicationService);
            this.panel3.Location = new System.Drawing.Point(483, 14);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(309, 262);
            this.panel3.TabIndex = 28;
            // 
            // btnTestAuditDataReplicationService
            // 
            this.btnTestAuditDataReplicationService.Location = new System.Drawing.Point(15, 75);
            this.btnTestAuditDataReplicationService.Name = "btnTestAuditDataReplicationService";
            this.btnTestAuditDataReplicationService.Size = new System.Drawing.Size(278, 38);
            this.btnTestAuditDataReplicationService.TabIndex = 12;
            this.btnTestAuditDataReplicationService.Text = "Test \"Audit/Real Time\" Data Replication Service (8732)";
            this.btnTestAuditDataReplicationService.UseVisualStyleBackColor = true;
            this.btnTestAuditDataReplicationService.Click += new System.EventHandler(this.btnTestAuditDataReplicationService_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(11, 62);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(1123, 166);
            this.webBrowser1.TabIndex = 30;
            this.webBrowser1.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtDataRepUrl);
            this.panel4.Controls.Add(this.webBrowser1);
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Location = new System.Drawing.Point(12, 295);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1150, 250);
            this.panel4.TabIndex = 30;
            // 
            // txtDataRepUrl
            // 
            this.txtDataRepUrl.Location = new System.Drawing.Point(11, 26);
            this.txtDataRepUrl.Name = "txtDataRepUrl";
            this.txtDataRepUrl.ReadOnly = true;
            this.txtDataRepUrl.Size = new System.Drawing.Size(769, 20);
            this.txtDataRepUrl.TabIndex = 31;
            this.txtDataRepUrl.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 289);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Results";
            // 
            // QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 586);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboxEnvironment);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Name = "QueryForm";
            this.Text = "Query Form";
            this.Load += new System.EventHandler(this.QueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cboxEnvironment;
        private System.Windows.Forms.ComboBox cboxLocation;
        private System.Windows.Forms.TextBox txtPingFeedback;
        private System.Windows.Forms.TextBox txtComputerName;
        private System.Windows.Forms.Button btnTestDataReplicationService;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDivision;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDistrict;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSql;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox cboxSql;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnTestAuditDataReplicationService;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtDataRepUrl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
    }
}

