﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.NetworkInformation;

namespace ApexTicketingMachinesHelper
{
    public partial class QueryForm : Form
    {
        private string _environment;
        private string _sql;
        private string _computername;
        private string _division;
        private string _district;
        private string _locationid;
        private string _description;
        private string _connStrTicketingSystem;
        private string _connStrSSISServer;
        private string _connStrApexCentralAdmin;
        private DataSet _ds;
        

        public QueryForm()
        {
            InitializeComponent();

        }

        private void QueryForm_Load(object sender, EventArgs e)
        {
            _ds = new DataSet();

            _environment = "PROD";
            cboxEnvironment.SelectedItem = "PROD";

            setupEnvironment();
            setupQueries();
        }

        private void resetControls()
        {
            dataGridView1.Visible = false;
            txtDataRepUrl.Visible = false;
            webBrowser1.Visible = false;
            //txtPingFeedback.Text = "";
            cboxSql.SelectedValue = "";
            txtSql.Text = "";

            //txtSQLFeedback.Text = "";
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
        }


        private void cboxEnvironment_SelectedValueChanged(object sender, EventArgs e)
        {
            _environment = this.cboxEnvironment.SelectedItem.ToString();
            resetControls();
            setupEnvironment();

        }

        private void cboxLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            _locationid = cboxLocation.SelectedValue.ToString();
            resetControls();
            setupTicketingSystemInfo();
        }
        
        private void setupQueries()
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.TableName = "SqlQueries";
            dt.Columns.Add("Id");
            dt.Columns.Add("Display");
            dt.Columns.Add("Query");
            
            DataRow _q0 = dt.NewRow();
            _q0["Id"] = "0";
            _q0["Display"] = "Ad Hoc";
            _q0["Query"] = "";
            dt.Rows.Add(_q0);

            DataRow _q1 = dt.NewRow();
            _q1["Id"] = "1";
            _q1["Display"] = "show locations (Inlocat)";
            _q1["Query"] = "select @@SERVERNAME as SERVERNAME, * from Inlocat";
            dt.Rows.Add(_q1);

            DataRow _q2 = dt.NewRow();
            _q2["Id"] = "2";
            _q2["Display"] = "show document template configurations for imaging (Syjtp)";
            _q2["Query"] = "select @@SERVERNAME as SERVERNAME, * from Syjtp";
            dt.Rows.Add(_q2);

            DataRow _q3 = dt.NewRow();
            _q3["Id"] = "3";
            _q3["Display"] = "show today's real time scale tickets (tkscale)";
            _q3["Query"] = "select @@SERVERNAME as SERVERNAME, * from tkscale where ticketdate = cast(getdate() as date)";
            dt.Rows.Add(_q3);

            DataRow _q4 = dt.NewRow();
            _q4["Id"] = "4";
            _q4["Display"] = "show today's real time misc tickets (tksmisc)";
            _q4["Query"] = "select @@SERVERNAME as SERVERNAME, * from tksmisc where ticketdate = cast(getdate() as date)";
            dt.Rows.Add(_q4);

            DataRow _q5 = dt.NewRow();
            _q5["Id"] = "5";
            _q5["Display"] = "show today's real time rail tickets (rrscale)";
            _q5["Query"] = "select @@SERVERNAME as SERVERNAME, * from rrscale where ticketdate = cast(getdate() as date)";
            dt.Rows.Add(_q5);

            DataRow _q6 = dt.NewRow();
            _q6["Id"] = "6";
            _q6["Display"] = "show today's end of day tickets (tkhist1)";
            _q6["Query"] = "select @@SERVERNAME as SERVERNAME, * from tkhist1 where ticketdate = cast(getdate() as date)";
            dt.Rows.Add(_q6);

            DataRow _q7 = dt.NewRow();
            _q7["Id"] = "7";
            _q7["Display"] = "show ticket recon reporting table (TicketRecon)";
            _q7["Query"] = "select @@SERVERNAME as SERVERNAME, * from TicketRecon";
            dt.Rows.Add(_q7);

            DataRow _q8 = dt.NewRow();
            _q8["Id"] = "8";
            _q8["Display"] = "show accumulator table (accum_delta)";
            _q8["Query"] = "select @@SERVERNAME as SERVERNAME, * from accum_delta";
            dt.Rows.Add(_q8);

            DataRow _q9 = dt.NewRow();
            _q9["Id"] = "9";
            _q9["Display"] = "show today's end of day data replication table (sydatrep)";
            _q9["Query"] = "select @@SERVERNAME as SERVERNAME, * from sydatrep where cast(CreateDateTime as date) = cast(getdate() as date)";
            dt.Rows.Add(_q9);

            DataRow _q10 = dt.NewRow();
            _q10["Id"] = "10";
            _q10["Display"] = "show today's real time data replication table (audit - sydatrep)";
            _q10["Query"] = "select @@SERVERNAME as SERVERNAME, * from jwsaudit1.dbo.sydatrep where cast(CreateDateTime as date) = cast(getdate() as date)";
            dt.Rows.Add(_q10);

            DataRow _q11 = dt.NewRow();
            _q11["Id"] = "11";
            _q11["Display"] = "show end of day data replication triggers (sys.triggers)";
            _q11["Query"] = "select @@SERVERNAME as SERVERNAME, * from sys.triggers where name like '%JWSInsert%' or name like '%JWSUpdate%'";
            dt.Rows.Add(_q11);

            DataRow _q12 = dt.NewRow();
            _q12["Id"] = "12";
            _q12["Display"] = "show real time data replication triggers (audit - sys.triggers)";
            _q12["Query"] = "select @@SERVERNAME as SERVERNAME, * from jwsaudit1.sys.triggers where name like '%JWSInsert%' or name like '%JWSUpdate%'";
            dt.Rows.Add(_q12);

            DataRow _q13 = dt.NewRow();
            _q13["Id"] = "13";
            _q13["Display"] = "show custom stored procedures (sys.procedures)";
            _q13["Query"] = "select @@SERVERNAME as SERVERNAME, * from sys.procedures";
            dt.Rows.Add(_q13);

            DataRow _q14 = dt.NewRow();
            _q14["Id"] = "14";
            _q14["Display"] = "show ticket streams (inlocat)";
            _q14["Query"] = "select @@SERVERNAME as SERVERNAME,LocationID, Description, MinTicketNo, MaxTicketNo, NextTicketNo, NextBlNo, NextMiscNo, * from Inlocat";
            dt.Rows.Add(_q14);

            DataRow _q15 = dt.NewRow();
            _q15["Id"] = "15";
            _q15["Display"] = "compare accumulator.updatevalue to salesorder.shipqty";
            _q15["Query"] = "select @@SERVERNAME as SERVERNAME, po.ShipQty, a.UpdatedValue,a.UpdateType, po.OrderID, po.LocationID, po.ProductID, a.Delta, a.DateApplied, a.InitialValue, a.UpdatedValue, a.DateModified, a.DateExtracted " +
                                "from slorder po, accum_delta a " +
                                "where po.OrderID = a.OrderID " +
                                 " and po.LocationID = a.LocationID " +
                                 " and po.ProductID = a.ProductID " +
                                "order by po.OrderID, po.LocationID, po.ProductID,a.UpdateType desc";
            dt.Rows.Add(_q15);

            //DataRow _q16 = dt.NewRow();
            //_q14["Id"] = "16";
            //_q14["Display"] = "compare accumulator.updatevalue does not match salesorder.shipqty";
            //_q14["Query"] = "select @@SERVERNAME as SERVERNAME, po.ShipQty, a.UpdatedValue, po.OrderID, po.LocationID, po.ProductID, a.* " +
            //                    "from slorder po, accum_delta a " +
            //                    "where po.OrderID = a.OrderID " +
            //                     " and po.LocationID = a.LocationID " +
            //                     " and po.ProductID = a.ProductID " +
            //                    "order by po.OrderID, po.LocationID, po.ProductID,a.UpdateType desc";
            //dt.Rows.Add(_q16);

            //cboxSql.Items.Add("show document template configuration(Syjtp)");
            //cboxSql.Items.Add("show locations(Inlocat)");
            //cboxSql.Items.Add("show ticket recon(TicketRecon)");

            _ds.Tables.Add(dt);

            cboxSql.DataSource = _ds.Tables["SqlQueries"];
            cboxSql.DisplayMember = "Display";
            cboxSql.ValueMember = "Query";
        }

        private void setupTicketingSystemInfo()
        {
            DataTable destinations = _ds.Tables["Destinations"];
            DataTable businessunits = _ds.Tables["BusinessUnits"];

            //var query = (
            //    from destination in destinations.AsEnumerable()
            //    where destination.Field<string>("LocationID") == cboxLocation.SelectedValue.ToString()
            //    select new
            //    {
            //        ComputerName = destination.Field<string>("ComputerName")
            //    }).ToList();

            var query =
                from destination in destinations.AsEnumerable()
                join businessunit in businessunits.AsEnumerable()
                on destination.Field<string>("LocationID") equals
                    businessunit.Field<string>("LocationID")
                where destination.Field<string>("LocationID") == cboxLocation.SelectedValue.ToString()
                select new
                {
                    LocationID = destination.Field<string>("LocationID"),
                    ComputerName = destination.Field<string>("ComputerName"),
                    DistrictID = businessunit.Field<string>("DistrictID"),
                    DivisionID = businessunit.Field<string>("DivisionID"),
                    Description = businessunit.Field<string>("Description"),
                    Display = businessunit.Field<string>("DivisionID") + " - " +
                                  businessunit.Field<string>("DistrictID") + " - " +
                                  businessunit.Field<string>("LocationID") + " - " +
                                  businessunit.Field<string>("Description")
                };

            foreach (var cn in query)
            { 
                _computername = cn.ComputerName;
                _district = cn.DistrictID;
                _division = cn.DivisionID;
                _description = cn.Description;
            }
            this.txtComputerName.Text = _computername;
            this.txtLocation.Text = _locationid;
            this.txtDivision.Text = _division;
            this.txtDistrict.Text = _district;
            this.txtDescription.Text = _description;
            this.txtPingFeedback.Text = getIPAddress(); 


            if (_environment == "PROD")
            {
                this._connStrTicketingSystem = "server=" + _computername + @"\jwsapex;database=JwsData1;UID=jwsapex;password=Alw@y5Y3sAnDNo";
            }
            if (_environment == "QA")
            {
                this._connStrTicketingSystem = "server=" + _computername + @"\jwsapex;database=JwsData1;UID=jwsapex;password=jwsapex";
            }
            if (_environment == "INT")
            {
                this._connStrTicketingSystem = "server=" + _computername + @"\jwsapex;database=JwsData1;UID=jwsapex;password=jwsapex";
            }

        }

        private void setupEnvironment()
        {

            //txtSQLFeedback.Text = "";
            txtPingFeedback.Text = "";

            if (_environment == "PROD")
            {
                this._connStrSSISServer = "server=SSIS01;database=Apex;UID=ssisuser;password=martin1";
                this._connStrApexCentralAdmin = "server=APXDBLADM01;database=JWSDataCentralAdm;UID=jwsapex;password=R35p3ctth3PL@n!";
            }
            if (_environment == "QA")
            {
                this._connStrSSISServer = "server=SSIS01QA;database=Apex;UID=ssisuser;password=martin1";
                this._connStrApexCentralAdmin = "server=APXDBLADM01QA;database=JWSDataCentralAdm;UID=jwsapex;password=jwsapex";
            }
            if (_environment == "INT")
            {
                this._connStrSSISServer = "server=SSIS01INT;database=Apex;UID=ssisuser;password=martin1";
                this._connStrApexCentralAdmin = "server=RALAPXSQL01INT;database=JWSDataCentralAdm;UID=jwsapex;password=jwsapex";
            }

            if (_ds.Tables.Contains("Destinations"))
                _ds.Tables.Remove("Destinations");

            if (_ds.Tables.Contains("BusinessUnits"))
                _ds.Tables.Remove("BusinessUnits");

            SqlDataAdapter daDest = new SqlDataAdapter("SELECT ID as LocationID, ComputerName FROM MMMDataRepDestinations Where SystemID = 'CENADM' and ID not like 'CENDIS%' and ID not like 'JWSIDB%'", _connStrSSISServer);
            daDest.Fill(_ds, "Destinations");
            DataTable destinations = _ds.Tables["Destinations"];
           
            SqlDataAdapter daBU = new SqlDataAdapter("select LocationID, DistrictID, DivisionID, Description from BusinessUnits", _connStrApexCentralAdmin);
            daBU.Fill(_ds, "BusinessUnits");
            DataTable businessunits = _ds.Tables["BusinessUnits"];

            var query =
                from destination in destinations.AsEnumerable()
                join businessunit in businessunits.AsEnumerable()
                on destination.Field<string>("LocationID") equals
                    businessunit.Field<string>("LocationID")
                orderby businessunit.Field<string>("DivisionID"),
                        businessunit.Field<string>("DistrictID"),
                        businessunit.Field<string>("LocationID")
                select new
                {
                    LocationID = destination.Field<string>("LocationID"),
                    ComputerName = destination.Field<string>("ComputerName"),
                    DistrictID = businessunit.Field<string>("DistrictID"),
                    DivisionID = businessunit.Field<string>("DivisionID"),
                    Description = businessunit.Field<string>("Description"),
                    Display = businessunit.Field<string>("DivisionID") + " - " + 
                                  businessunit.Field<string>("DistrictID") + " - " +
                                  businessunit.Field<string>("LocationID") + " - " +
                                  businessunit.Field<string>("Description")
                };

            cboxLocation.DataSource = query.ToList();
            cboxLocation.DisplayMember = "Display";
            cboxLocation.ValueMember = "LocationID";
            
        }


        private void cboxSql_SelectedValueChanged(object sender, EventArgs e)
        {
            this.txtSql.Text = this.cboxSql.SelectedValue.ToString();
            //txtSQLFeedback.Text = "";
            //_sql = this.cboxSql.SelectedValue.ToString();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            txtDataRepUrl.Visible = false;
            webBrowser1.Visible = false;

            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            _sql = this.txtSql.Text;

            if ( (_sql.ToUpper().Contains("DELETE")) ||
                 (_sql.Trim().ToUpper().Substring(0, 6) == "INSERT") ||  //checks for INSERT at begining of the sql string
                 (_sql.Trim().ToUpper().Substring(0, 6) == "UPDATE") ||  //checks for UPDATE at begining of the sql string
                 (_sql.ToUpper().Contains("TRUNCATE")) ||
                 (_sql.ToUpper().Contains("DROP")) ||
                 (_sql.ToUpper().Contains("MERGE")) ||
                 (_sql.ToUpper().Contains("EXEC")) )
            {
                string message = "This application supports SELECT queries. \nIt does not support INSERT, UPDATE, or DELETE.";
                string title = "Query Error";
                MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (_sql == "")
            {
                //MessageBox.Show("Please define a query");
                string message = "Please define a query";
                string title = "Query Error";
                MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            



            setupTicketingSystemInfo();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(_sql, _connStrTicketingSystem);
                DataSet ds = new DataSet();
                da.Fill(ds, "table");
                dataGridView1.DataSource = ds.Tables["table"].DefaultView;
                dataGridView1.Visible = true;
            }
            catch (Exception ex)
            {
                //txtSQLFeedback.Text = ex.Message; 
                string message = ex.Message;
                string title = "Query Error";
                MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }

        }

        private string getIPAddress()
        {
            string addr = "";
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send(_computername, 1000);
                if (reply != null && reply.Address != null)
                {
                    addr = reply.Address.ToString();
                }
                else
                {
                    addr = reply.Status.ToString();
                }
            }
            catch
            {
                addr = "ERROR: unable to ping";
            }
            return addr;
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            //txtPingFeedback.Text = "";
            //txtSQLFeedback.Text = "";
            //dataGridView1.DataSource = null;
            //dataGridView1.Rows.Clear();
            resetControls();

        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send(_computername, 1000);
                if (reply != null)
                {
                    txtPingFeedback.Text = "Status :  " + reply.Status + " \n Time : " + reply.RoundtripTime.ToString() + " \n Address : " + reply.Address;
                }
            }
            catch
            {
                txtPingFeedback.Text = "ERROR: unable to ping - some timeout issue";
            }
        }

        private void btnTestDataReplicationService_Click(object sender, EventArgs e)
        {
            //Process.Start("http://"+_computername+":8731/Cai.Apex.Services.DataReplicationUpdate.DataReplicationUpdateService");
            dataGridView1.Visible = false;
            txtDataRepUrl.Visible = true;
            webBrowser1.Visible = true;
            string url = "http://" + _computername + ":8731/Cai.Apex.Services.DataReplicationUpdate.DataReplicationUpdateService";
            txtDataRepUrl.Text = url;
            webBrowser1.Navigate(url);


        }

        private void btnTestAuditDataReplicationService_Click(object sender, EventArgs e)
        {
            //Process.Start("http://" + _computername + ":8732/Cai.Apex.Services.DataReplicationUpdate.DataReplicationUpdateService");
            dataGridView1.Visible = false;
            txtDataRepUrl.Visible = true;
            webBrowser1.Visible = true;
            string url = "http://" + _computername + ":8732/Cai.Apex.Services.DataReplicationUpdate.DataReplicationUpdateService";
            txtDataRepUrl.Text = url; 
            webBrowser1.Navigate(url);
        }
    }
}
